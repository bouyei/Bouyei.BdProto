﻿using System;
using System.Text;
using Bouyei.BdProtoCore;
using Bouyei.BdProtoCore.JT808;
using Bouyei.BdProtoCore.JT808.Reponse;
using Bouyei.BdProtoCore.Providers;
using Bouyei.BdProtoCore.Structures;

namespace Bouyei.BdProtoCoreDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Decode();
        }

        private static void Decode()
        {
            byte[] arry = new byte[] {
            0x7E, 0x01, 0x00, 0x00, 0x2D, 0x01, 0x87, 0x56, 0x93, 0x85,
                0x86, 0x00, 0x00, 0x00, 0x15, 0x00, 0xC8, 0x37, 0x32,
                0x36, 0x30, 0x34, 0x52, 0x4E, 0x2D, 0x43, 0x41, 0x2D,
                0x50, 0x35, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x32, 0x32, 0x30,
                0x30, 0x33, 0x30, 0x02, 0xD4, 0xC1, 0x42, 0x37, 0x31,
                0x30, 0x37, 0x30, 0xAC, 0x7E
            };

            IPacketProvider pg = PacketProvider.CreateProvider();
            //var msg = pg.Decode(arry, 0, arry.Length);
            //if (msg.pmPacketHead.phMessageId == JT808Cmd.RSP_0100)
            //{
            //    PB0100 bodyInfo = new REP_0100().Decode(msg.pmMessageBody);
            //}

            RspPacketFrom from = new  RspPacketFrom(pg);
            var rsp=from.Decode<PB0100>(arry);
        }
    }
}
